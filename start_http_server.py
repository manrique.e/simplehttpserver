import http.server
import datetime
import socket

date = datetime.datetime.now()
hostname = socket.gethostname()

class MyHandler(http.server.BaseHTTPRequestHandler): 
    def do_GET(self):
        self.send_response(200)
        self.end_headers()
        mySmallPage = "<html>\n"
        mySmallPage += "<head><title>A simple Program</title></head><body>\n"
        mySmallPage += "<body>\n"    
        mySmallPage += "<h1>This is a kubernetes test</h1>\n"
        mySmallPage +="<div>This test is being done on the following date: <br />\n"
        mySmallPage += "<b>" +  str(date) + "</b> <br />\n"
        mySmallPage += "and it's being running in this host: <br />\n "
        mySmallPage += "<b>" + str(hostname) + "<b></div>\n"
        mySmallPage += "</body></html>\n"    
        b = bytes(mySmallPage,'utf-8')
        self.wfile.write(b)
if __name__ == "__main__":
    server = http.server.HTTPServer(('', 8080), MyHandler) 
    server.serve_forever()
